package com.example.ngider1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DashboardActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_new);

        Button btnseller=findViewById(R.id.buttonpenjual);
        btnseller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent etnseller=new Intent(DashboardActivity.this, SellerActivity.class);
                startActivity(etnseller);
            }
        });

        Button btnbuyer=findViewById(R.id.buttonpembeli);
        btnbuyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent etnbuyer=new Intent(DashboardActivity.this, ProfileActivity.class);
                startActivity(etnbuyer);
            }
        });
    }
}
