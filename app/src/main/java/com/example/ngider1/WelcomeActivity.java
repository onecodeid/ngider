package com.example.ngider1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class WelcomeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        CardView cardwelcome=findViewById(R.id.welcomebutton);
        cardwelcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent crdwelcome=new Intent(WelcomeActivity.this, LoginPage.class);
                startActivity(crdwelcome);
            }
        });
    }
}
